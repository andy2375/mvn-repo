<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.merryapps</groupId>
	<artifactId>WallNow-parent</artifactId>
	<version>1.0-SNAPSHOT</version>
	<packaging>pom</packaging>
	<name>WallNow - Parent</name>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<platform.version> 4.1.1.4</platform.version>
		<android.plugin.version>3.6.0</android.plugin.version>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>com.google.android</groupId>
				<artifactId>android</artifactId>
				<version>${platform.version}</version>
				 <scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>com.google.android</groupId>
				<artifactId>android-test</artifactId>
				<version>${platform.version}</version>
				 <scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>junit</groupId>
				<artifactId>junit</artifactId>
				<version>4.11</version>
				<exclusions>
		        <exclusion>
		            <artifactId>hamcrest-core</artifactId>
		            <groupId>org.hamcrest</groupId>
		        </exclusion>
		    	</exclusions>
			</dependency>
			
		</dependencies>
	</dependencyManagement>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<artifactId>maven-jarsigner-plugin</artifactId>
					<version>1.2</version>
				</plugin>
				<plugin>
					<artifactId>maven-resources-plugin</artifactId>
					<version>2.6</version>
					<configuration>
						<encoding>UTF-8</encoding>
					</configuration>
				</plugin>

				<plugin>
					<groupId>com.jayway.maven.plugins.android.generation2</groupId>
					<artifactId>android-maven-plugin</artifactId>
					<version>${android.plugin.version}</version>
					<configuration>
						<sdk>
							<platform>19</platform>
							
						</sdk>
						<release>true</release>
						<emulator>
							<avd>avd 18</avd>
						</emulator>
												<zipalign>
							<verbose>true</verbose>
						</zipalign>
						<undeployBeforeDeploy>true</undeployBeforeDeploy>
						<extractDuplicates>true</extractDuplicates>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>build-helper-maven-plugin</artifactId>
					<version>1.8</version>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<profiles>
		<profile>
			<!-- the standard profile runs the instrumentation tests -->
			<id>standard</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<modules> 
				<module>WallNow</module>
				<module>WallNow-it</module>
			</modules>
		</profile>
		<profile>
			<!-- the release profile does sign, proguard, zipalign ... but does not 
				run instrumentation tests -->
			<id>release</id>
			<!-- via this activation the profile is automatically used when the release 
				is done with the maven release plugin -->
			<activation>
				<property>
					<name>performRelease</name>
					<value>true</value>
				</property>
			</activation>
			<modules>
				<module>WallNow</module>
			</modules>
		</profile>
	</profiles>

  <modules>
    <module>WallNow</module>
    <module>WallNow-it</module>
  </modules>
</project>
